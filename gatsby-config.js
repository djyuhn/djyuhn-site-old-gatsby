module.exports = {
    plugins: [
        {
            resolve: "gatsby-theme-blog-core",
            options: { basePath: "/blog" }
        },
        "gatsby-plugin-react-helmet",
        "gatsby-plugin-typescript",
        {
            resolve: "gatsby-plugin-codegen",
            options: {
                localSchemaFile: "generated/graphql-schema.json",
                output: "generated/graphql-types.d.ts",
                outputFlat: true
            }
        },
        "gatsby-plugin-netlify-cms",
        "gatsby-plugin-sass",
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: "DJ Yuhn Personal Site",
                short_name: "Yuhn Site",
                start_url: "/",
                background_color: "#6b37bf",
                theme_color: "#6b37bf",
                icons: [
                    {
                        src: "static/images/favicon-16x16.png",
                        sizes: "16x16",
                        type: "image/png"
                    }
                ],
                // Enables "Add to Homescreen" prompt and disables browser UI (including back button)
                // see https://developers.google.com/web/fundamentals/web-app-manifest/#display
                display: "standalone",
                icon: "static/images/favicon-32x32.png" // This path is relative to the root of the site.
            }
        }
    ],
    siteMetadata: {
        title: "DJ Yuhn",
        author: "DJ Yuhn",
        description: "Personal site for DJ Yuhn.",
        social: [
            {
                name: "twitter",
                url: "https://twitter.com/djyuhn"
            },
            {
                name: "github",
                url: "https://github.com/djyuhn"
            },
            {
                name: "gitlab",
                url: "https://gitlab.com/djyuhn"
            }
        ]
    }
};
