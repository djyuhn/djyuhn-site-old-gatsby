/** @jsx jsx */

import { jsx } from "@emotion/core";

import { H1, P, Theme } from "../theme";

import Layout from "../components/layout";
import SEO from "../components/seo";

import { PageProps } from "../types/site";
import Container from "../components/container";
import { Box, Flex, Link, List, ListItem, Text } from "@chakra-ui/core/dist";
import { useTheme } from "emotion-theming";

const About = ({ location }: PageProps) => {
    const theme = useTheme<Theme>();
    return (
        <Layout path={location.pathname}>
            <SEO title="About" />
            <Container>
                <Flex>
                    <Box>
                        <H1>About</H1>
                        <Text as={P}>
                            I am DJ Yuhn, a software developer and a former medical technologist. I currently work as a
                            software engineer for{" "}
                            <Link
                                target={"_blank"}
                                color={theme.colors.linkColor}
                                fontFamily={theme.fonts.serif}
                                href="https://artisantechgroup.com"
                            >
                                Artisan Technology Group
                            </Link>{" "}
                            in Kansas City.
                        </Text>
                        <Text as={P}>
                            I created this website to begin learning React and to document all the subjects I have been
                            learning.
                        </Text>
                    </Box>
                </Flex>
                <Flex>
                    <Box>
                        <H1>Skills</H1>
                        <List styleType={"disc"}>
                            <ListItem fontFamily={theme.fonts.serif}>
                                <b>Languages:</b> Java, Scala, Python, Typescript, Kotlin, C#
                            </ListItem>
                            <ListItem fontFamily={theme.fonts.serif}>
                                <b>Database:</b> MySQL, Firebase, MongoDB, PostgreSQL
                            </ListItem>
                            <ListItem fontFamily={theme.fonts.serif}>
                                <b>Tools:</b> Visual Studio, Visio, Git
                            </ListItem>
                            <ListItem fontFamily={theme.fonts.serif}>
                                <b>Frameworks:</b> .NET, Stanford CoreNLP, Apache Spark, Angular{" "}
                            </ListItem>
                            <ListItem fontFamily={theme.fonts.serif}>
                                <b>Virtualization:</b> Docker, Oracle VirtualBox{" "}
                            </ListItem>
                        </List>
                    </Box>
                </Flex>
                <Flex>
                    <Box>
                        <H1>Currently Using</H1>
                        <List styleType={"disc"}>
                            <ListItem fontFamily={theme.fonts.serif}>
                                <b>Computer:</b> Lenovo X1 Carbon 6th Gen{" "}
                            </ListItem>
                            <ListItem fontFamily={theme.fonts.serif}>
                                <b>Hosting:</b>{" "}
                                <Link
                                    color={theme.colors.linkColor}
                                    fontFamily={theme.fonts.serif}
                                    href="https://www.netlify.com"
                                >
                                    Netlify
                                </Link>{" "}
                                (blog)
                            </ListItem>
                            <ListItem fontFamily={theme.fonts.serif}>
                                <b>Development Tools:</b>{" "}
                                <Link
                                    color={theme.colors.linkColor}
                                    fontFamily={theme.fonts.serif}
                                    href="https://www.jetbrains.com"
                                >
                                    Jetbrains Suite
                                </Link>
                            </ListItem>
                        </List>
                    </Box>
                </Flex>
            </Container>
        </Layout>
    );
};

export default About;
