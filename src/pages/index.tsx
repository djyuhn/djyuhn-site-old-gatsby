/** @jsx jsx */

import { jsx } from "@emotion/core";

import { PageProps } from "../types/site";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Container from "../components/container";
import { Box, Flex, Link, Text } from "@chakra-ui/core/dist";
import { H1, P, Theme } from "../theme";
import { useTheme } from "emotion-theming";

const Home = ({ location }: PageProps) => {
    const theme = useTheme<Theme>();
    return (
        <Layout path={location.pathname}>
            <SEO title="Home"/>{" "}
            <Container>
                <Flex>
                    <Box>
                        <H1>Hey there, I'm DJ</H1>
                        <Text as={P}>
                            I am a software developer based in Kansas City and I have worked with a variety of
                            technologies including: Azure, Azure DevOps, .NET, Angular, Typescript. I have been working
                            as a Software Engineer since April 2019 and I completed my master's degree in Computer
                            Science in December 2019. During the day I work on Windows but at home I use a Linux distro
                            called{" "}
                            <Link
                                target={"_blank"}
                                color={theme.colors.linkColor}
                                fontFamily={theme.fonts.serif}
                                href="https://getsol.us/home/"
                            >
                                Solus.
                            </Link>
                        </Text>
                    </Box>
                </Flex>
            </Container>
        </Layout>
    );
};

export default Home;
