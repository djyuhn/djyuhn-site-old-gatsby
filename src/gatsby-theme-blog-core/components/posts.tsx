import React from "react";
import Posts from "../../components/posts";
import { BlogPost } from "../../types/blog";
import { SiteLocation, Social } from "../../types/site";

interface PostsCoreProps {
    location: SiteLocation;
    data: {
        allBlogPost: {
            edges: { node: BlogPost }[];
        };
        site: {
            siteMetadata: {
                social: Social[];
                title: string;
            };
        };
    };
}

const PostsCore = ({ location, data }: PostsCoreProps) => {
    const { site, allBlogPost } = data;
    return (
        <Posts
            location={location}
            posts={allBlogPost.edges}
            siteTitle={site.siteMetadata.title}
            socialLinks={site.siteMetadata.social}
        />
    );
};

export default PostsCore;
