import React from "react";
import Post from "../../components/post";
import { BlogPost } from "../../types/blog";
import { SiteLocation, SiteMetadata } from "../../types/site";

interface PostCoreProps {
    location: SiteLocation;
    data: {
        blogPost: BlogPost;
        previous: BlogPost;
        next: BlogPost;
        site: {
            siteMetadata: SiteMetadata;
        };
    };
}

const PostCore = ({ location, data }: PostCoreProps) => {
    return (
        <Post data={{ post: data.blogPost, ...data }} location={location} previous={data.previous} next={data.next} />
    );
};

export default PostCore;
