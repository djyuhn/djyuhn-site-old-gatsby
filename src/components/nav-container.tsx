/** @jsx jsx */

import { jsx } from "@emotion/core";
import { Theme } from "../theme";
import { useTheme } from "emotion-theming";
import Container from "./container";
import React from "react";

const NavContainer = (props: React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>) => {
    const theme = useTheme<Theme>();

    return (
        <Container
            css={{
                height: "100%",
                maxWidth: theme.sizes.containerWidth,
                justifyContent: "space-between",
                transition: "height .3s ease",
                display: "flex",
                alignItems: "center",
                padding: `0 ${theme.sizes.universalPadding}`
            }}
            {...props}
        />
    );
};

export default NavContainer;
