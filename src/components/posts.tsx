/** @jsx jsx */

import { jsx } from "@emotion/core";

import { BlogPost } from "../types/blog";
import { SiteLocation, Social } from "../types/site";

import Layout from "./layout";
import Link from "./link";
import SEO from "./seo";
import { Box } from "@chakra-ui/core/dist";
import Container from "./container";
import Card from "./card";

interface PostsProps {
    location: SiteLocation;
    posts: { node: BlogPost }[];
    siteTitle: string;
    socialLinks: Social[];
}

const Posts = ({ location, posts, siteTitle: _siteTitle, socialLinks: _socialLinks }: PostsProps) => {
    return (
        <Layout path={location.pathname}>
            <SEO title="All Posts" keywords={["blog", "gatsby", "javascript", "react"]} />
            <Container>
                <Box>
                    {posts.map(({ node }) => {
                        const title = <Link to={node.slug}>{node.title || node.slug}</Link>;
                        //const keywords = node.keywords || [];
                        return <Card key={node.slug} title={title} date={node.date} description={node.excerpt} />;
                    })}
                </Box>
            </Container>
        </Layout>
    );
};

export default Posts;
