/** @jsx jsx */

import { jsx } from "@emotion/core";
import { Link as GatsbyLink } from "gatsby";
import css from "@emotion/css";
import { useTheme } from "emotion-theming";
import { Theme } from "../theme";
import { Flex, Link } from "@chakra-ui/core/dist";
import NavContainer from "./nav-container";
import Container from "./container";

const Logo = () => {
    const theme = useTheme<Theme>();
    return (
        <img
            alt="Logo"
            src="/images/logo.png"
            css={{ alignSelf: "center", padding: `0 ${theme.sizes.universalPadding}` }}
        />
    );
};

const Header = () => {
    const theme = useTheme<Theme>();

    const headerStyle = css({
        // Always apply background color to avoid shine through
        background: theme.colors.headerBackground,
        color: theme.colors.headerText,
        borderBottom: `${theme.sizes.border} solid ${theme.colors.headerBorder}`,
        height: theme.sizes.headerHeight,
        position: "sticky"
    });

    const linksContainer = css({
        justifyContent: "flex-end",
        padding: `0, ${theme.sizes.universalPadding}`,
        alignItems: "center"
    });

    const linkStyle = css({
        color: theme.colors.headerText,
        alignSelf: "center",
        padding: `0 ${theme.sizes.navLinkPadding}`,
        textDecoration: "none",
        cursor: "pointer",
        width: "max-content",
        "&:hover, &:focus": {
            color: theme.colors.linkColor,
            textDecoration: "underline",
            background: theme.colors.headerHoverBackground,
            borderRadius: theme.radii.navLinkBorderRadius,
            transition: "all 0.5s ease"
        }
    });

    return (
        <Container css={headerStyle}>
            <NavContainer>
                <Flex css={linksContainer}>
                    <Logo />
                    <Link as={GatsbyLink} css={linkStyle} to={"/"}>
                        DJ Yuhn
                    </Link>
                </Flex>

                <Flex css={linksContainer}>
                    <Link as={GatsbyLink} css={linkStyle} to={"/about"}>
                        About
                    </Link>
                    <Link as={GatsbyLink} css={linkStyle} to={"/blog"}>
                        Blog
                    </Link>
                </Flex>
            </NavContainer>
        </Container>
    );
};

export default Header;
