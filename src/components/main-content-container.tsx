/** @jsx jsx */

import { jsx } from "@emotion/core";
import { Theme } from "../theme";
import { useTheme } from "emotion-theming";
import Container from "./container";

const MainContentContainer = (props: React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>) => {
    const theme = useTheme<Theme>();

    return (
        <Container
            css={{
                margin: `${theme.sizes.headerHeight} auto`,
                height: "100%",
                maxWidth: theme.sizes.containerWidth,
                justifyContent: "space-between",
                transition: "height .3s ease",
                alignItems: "center",
                padding: `0 ${theme.sizes.universalPadding}`
            }}
            {...props}
        />
    );
};

export default MainContentContainer;
