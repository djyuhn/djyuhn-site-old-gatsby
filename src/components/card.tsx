/** @jsx jsx */

import { jsx } from "@emotion/core";
import { Box, Flex, Image } from "@chakra-ui/core/dist";
import { H2, P, Theme } from "../theme";
import React from "react";
import { useTheme } from "emotion-theming";
import css from "@emotion/css";

interface CardProps {
    title: string | JSX.Element;
    date: string;
    description: string;
    imageUrl?: string;
}

const Card = (props: React.PropsWithChildren<CardProps>) => {
    const theme = useTheme<Theme>();

    const cardStyle = css({
        // Always apply background color to avoid shine through
        background: theme.colors.secondaryBackground,
        borderRadius: theme.radii.smallRadius,
        overflow: "hidden",
        boxShadow: theme.shadows.universalBoxShadow,
        color: theme.colors.headerText,
        margin: `${theme.sizes.universalMargin} 0`
    });

    const titleStyle = css({
        margin: theme.sizes.smallMargin
    });

    const dateStyle = css({
        margin: theme.sizes.smallMargin
    });

    const descriptionStyle = css({
        margin: theme.sizes.smallMargin
    });

    return (
        <Box css={cardStyle}>
            {props.imageUrl != null && <Image src={props.imageUrl} alt={"Card Cover"} />}
            <Box>
                <H2 css={titleStyle}>{props.title}</H2>
                <P css={dateStyle}>{props.date}</P>
                <P css={descriptionStyle}>{props.description}</P>
            </Box>
        </Box>
    );
};

export default Card;
