/** @jsx jsx */

import { jsx } from "@emotion/core";

const Container = (props: React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>) => {
    return (
        <div
            css={{
                margin: "auto"
            }}
            {...props}
        />
    );
};

export default Container;
