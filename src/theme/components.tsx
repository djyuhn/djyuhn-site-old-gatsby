/** @jsx jsx */

import { jsx } from "@emotion/core";
import { useTheme } from "emotion-theming";
import { SiteTheme, SubheadingTopMargin, UniversalMargin } from "./site-theme";
import React from "react";

const HeadingsStyle = {
    lineHeight: SiteTheme.lineHeights.heading,
    margin: `0 0`,
    fontWeight: SiteTheme.fontWeights.heading,
    fontFamily: SiteTheme.fonts.sansSerif,
    "&.small": {
        color: SiteTheme.colors.secondaryText,
        display: "block",
        marginTop: SubheadingTopMargin
    }
};

export const H1 = (props: React.DetailedHTMLProps<React.HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>) => (
    <h1 css={[HeadingsStyle, { fontSize: "2em" }]} {...props} />
);

export const H2 = (props: React.DetailedHTMLProps<React.HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>) => (
    <h2 css={[HeadingsStyle, { fontSize: "1.5em" }]} {...props} />
);

export const H3 = (props: React.DetailedHTMLProps<React.HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>) => (
    <h3 css={[HeadingsStyle, { fontSize: "1.25em" }]} {...props} />
);

export const H4 = (props: React.DetailedHTMLProps<React.HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement>) => (
    <h4 css={[HeadingsStyle, { fontSize: "1.125em" }]} {...props} />
);

export const P = (props: React.DetailedHTMLProps<React.HTMLAttributes<HTMLParagraphElement>, HTMLParagraphElement>) => (
    <p css={{ margin: UniversalMargin, fontSize: "1em", fontFamily: SiteTheme.fonts.serif }} {...props} />
);

export const A = (props: React.DetailedHTMLProps<React.AnchorHTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement>) => {
    const theme = useTheme<typeof SiteTheme>();
    return (
        <a
            css={{
                textDecoration: "none",
                "&:link": {
                    color: theme.colors.linkColor
                },
                "&:visited": {
                    color: theme.colors.linkVisitedColor
                },
                "&:hover, &:focus": {
                    textDecoration: "underline"
                },
                fontFamily: SiteTheme.fonts.serif,
                fontSize: "1em"
            }}
            {...props}
        />
    );
};
