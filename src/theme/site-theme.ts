import palx from "palx";
import { defaultGeneratePalette } from "./colors";
import { keyframes } from "@emotion/core";

export const BaseFontFamily =
    'Oxygen, Noto Serif, BlinkMacSystemFont, "Segoe UI", Roboto, Ubuntu, "Helvetica Neue", Helvetica, sans-serif';

export const SerifFont = "Noto Serif";
export const SansSerifFont = "Oxygen";
export const BaseLineHeight = 1.5;
export const BaseFontSize = "1em";
export const SubheadingTopMargin = "-0.25rem";
export const SmallFontSize = "0.75em";

export const UniversalMargin = "1rem";
export const UniversalPadding = "1rem";
export const NavLinkPadding = ".5rem";

const Breakpoints = ["320", "480", "720", "1280", "1440", "1680", "1920"];

const scales = palx("#1976d2");
const generatedColors = defaultGeneratePalette(scales);

export const SiteTheme = {
    generatedColors: { ...generatedColors },
    colors: {
        background: "#fff",
        text: "#111",
        secondaryText: "#444",
        secondaryBackground: "#f0f0f0",
        linkColor: "#0277bd",
        linkVisitedColor: "#01579b",
        preformatted: "#1565c0",
        border: "#aaa",
        secondaryBorder: "#ddd",
        headerBackground: "#f8f8f8",
        headerHoverBackground: "#e0e0e0",
        headerActiveBorder: "#1565c0",
        headerText: "#444",
        headerBorder: "#ddd",
        inputBackground: "#f8f8f8",
        inputText: "#111",
        inputBorder: "#ddd",
        inputFocus: "#0288d1",
        inputInvalid: "#d32f2f",
        buttonBackground: "#e2e2e2",
        buttonHoverBackground: "#dcdcdc",
        buttonText: "#212121",
        buttonBorder: "transparent",
        buttonHoverBorder: "transparent",
        buttonGroupBorder: "rgba(124,124,124, 0.54)",
        buttonPrimaryBackground: "#1976d2",
        buttonPrimaryHoverBackground: "#1565c0",
        buttonPrimaryText: "#f8f8f8"
    },
    fonts: {
        body: SerifFont,
        heading: SansSerifFont,
        monospace: "Menlo, Consolas, monospace",
        sansSerif: SansSerifFont,
        serif: SerifFont
    },
    fontWeights: {
        body: 400,
        heading: 600,
        bold: 700
    },
    lineHeights: {
        body: BaseLineHeight,
        heading: 1.2
    },
    fontSizes: {
        code: "0.85em"
    },
    sizes: {
        universalMargin: UniversalMargin,
        universalPadding: UniversalPadding,
        navLinkPadding: NavLinkPadding,
        containerWidth: "1000px",
        border: "0.0625rem",
        headerHeight: "3.1875rem",
        smallMargin: "0.5em",
        mediumMargin: "1em",
        largeMargin: "1.5em"
    },
    radii: {
        universalBorderRadius: "0.125rem",
        navLinkBorderRadius: "0.5rem",
        smallRadius: "1em"
    },
    shadows: {
        universalBoxShadow: "none"
    },
    breakpoints: Breakpoints,
    // Predefined media queries
    mediaQueries: {
        mobile: `@media screen and (min-width: ${Breakpoints[0]}px)`,
        mobileLandscape: `@media screen and (min-width: ${Breakpoints[1]}px)`,
        tablet: `@media screen and (min-width: ${Breakpoints[2]}px)`,
        tabletLandscape: `@media screen and (min-width: ${Breakpoints[3]}px)`,
        desktop: `@media screen and (min-width: ${Breakpoints[4]}px)`,
        desktopLarge: `@media screen and (min-width: ${Breakpoints[5]}px)`,
        desktopWide: `@media screen and (min-width: ${Breakpoints[6]}px)`
    },
    animations: {
        appear: keyframes`{
            0% {
            opacity: 0;
            }
            100% {
            opacity: 1;
            }
        }`
    }
};
