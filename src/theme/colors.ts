import open from "open-color";

/**
 * normalize open-color to remove some colours,
 * such as 'grape'. This should be compatible with a tool
 * like palx.
 */
const defaultScales = {
    gray: open.gray,
    blue: open.blue,
    green: open.green,
    red: open.red,
    orange: open.orange,
    yellow: open.yellow,
    teal: open.teal,
    cyan: open.cyan,
    lime: open.lime,
    pink: open.pink,
    violet: open.violet,
    indigo: open.indigo
};

export type ScalesType = typeof defaultScales;

type PaletteItem = {
    lightest: string;
    light: string;
    base: string;
    dark: string;
};

export type PaletteType = {
    gray: PaletteItem;
    blue: PaletteItem;
    red: PaletteItem;
    orange: PaletteItem;
    yellow: PaletteItem;
    green: PaletteItem;
    teal: PaletteItem;
    violet: PaletteItem;
};

/**
 * Generate a palette from scales.
 *
 * `light` is typically used in dark mode for things like
 *  outline buttons, ghost buttons, etc.
 *
 * `base` is typically used for buttons, avatar colors, etc.
 *
 * @param scales
 */
export function defaultGeneratePalette(scales: ScalesType): PaletteType {
    return {
        gray: {
            lightest: scales.gray[1],
            light: scales.gray[4],
            base: scales.gray[8],
            dark: scales.gray[9]
        },
        blue: {
            lightest: scales.blue[1],
            light: scales.blue[5],
            base: scales.blue[8],
            dark: scales.blue[9]
        },
        red: {
            lightest: scales.red[1],
            light: scales.red[6],
            base: scales.red[8],
            dark: scales.red[9]
        },
        orange: {
            lightest: scales.orange[1],
            light: scales.orange[4],
            base: scales.orange[8],
            dark: scales.orange[9]
        },
        yellow: {
            lightest: scales.yellow[1],
            light: scales.yellow[4],
            base: scales.yellow[8],
            dark: scales.yellow[9]
        },
        green: {
            lightest: scales.green[1],
            light: scales.green[5],
            base: scales.green[8],
            dark: scales.green[9]
        },
        teal: {
            lightest: scales.teal[1],
            light: scales.teal[4],
            base: scales.teal[8],
            dark: scales.teal[9]
        },
        violet: {
            lightest: scales.violet[1],
            light: scales.violet[4],
            base: scales.violet[8],
            dark: scales.violet[9]
        }
    };
}
